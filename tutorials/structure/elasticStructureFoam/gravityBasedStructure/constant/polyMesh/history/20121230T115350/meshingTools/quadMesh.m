function quadMesh(p1,p2,p3,p4)

if nargin < 1
    p1 = [0 0];
    p2 = [.6 0];
    p3 = [.5 .5];
    p4 = [.8 .3];
end

alpha = ([0:10]/10).^.6;
beta = ([0:10]/10).^.7;

figure(1); clf
p5 = lineSegment(p1,p2,alpha);
p6 = lineSegment(p3,p4,alpha);
for i = 1:length(alpha)
    p = lineSegment(p5(i,:),p6(i,:),beta);
    x(:,i) = p(:,1);
    y(:,i) = p(:,2);
end
plot(x,y,'.-')
hold on
plot(x',y','.-')
axis equal
drawnow
%When alpha and beta are linear smoothing does nothing.
%It is apparently already smooth
[x,y] = smoothen(x,y,1e-6);