/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNdU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOdUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICdULAR PdURPOSE.  See the GNdU General Public License
    for more details.

    You should have received a copy of the GNdU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 dUSA

Application
    soilEpTLFoam

Description
    Segregated finite-volume solver of large deformation, 
    small-strain deformation of a elasto-plastic soil material. Pore 
    pressure has been excluded in this solver.

    Under-relaxation method (e.g. Aitken's metho) can be chosen for 
    convergence acceleration due to the high nonlinearity of large 
    deformation and plastic strain.
    

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "Switch.H"

using namespace Foam;
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{

#   include "setRootCase.H"
#   include "createTime.H"
#   include "createMesh.H"
#   include "readSoilProperties.H"
#   include "createFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nCalculating displacement field\n" << endl;

    for (runTime++; !runTime.end(); runTime++)
    {
        Info<< "Time: " << runTime.timeName() << nl << endl;

        #   include "readSoilEpTLFoamControls.H"

        int iCorr = 0;
       
        lduMatrix::debug = 0;
        lduMatrix::solverPerformance solverPerf;
        scalar theta = 0.0;
        scalar initRes = 0.0;

        do 
          {
            iCorr++;
	   
            // store previous iteration, since later dU will be under-relaxed 
            dU.storePrevIter();
            res.storePrevIter();

            const volVectorField& dU_prevIter = dU.prevIter();
            const volVectorField& res_prevIter = res.prevIter();

            // solve the system with 'lagged' plasticity and cross-component coupling
            fvVectorMatrix dUEqn
            (
                fvm::d2dt2(rho, dU)
             ==
                fvm::laplacian(2*mu + lambda, dU, "laplacian(DU,dU)")

              + fvc::div
                (
                    - ((mu + lambda)*gradDU)
                    + (mu*gradDU.T()) 
                    + (mu*(gradDU & gradU.T()))
                    + (mu*(gradU & gradDU.T()))
                    + (mu*(gradDU & gradDU.T()))
                    + (lambda * tr(dEpsG) * I )
                    + (dSigma & gradU)
                    + ((sigma+dSigma) & gradDU),   
                    "div(sigma)"
                )

              - fvc::div(2.0*mu*(mesh.Sf() & fvc::interpolate(dEpsP)))

              - fvc::div(lambda*(mesh.Sf() & I*fvc::interpolate(tr(dEpsP))))               
            ); 

            solverPerf = dUEqn.solve();
            if (iCorr==1){initRes=gMax(mag(dU.internalField()));}

            res = (dU - dU_prevIter)/(initRes+SMALL);

            #  include "underRelaxationMethod.H"
            gradDU = fvc::grad(dU);
            DF = gradDU.T();
   
            // update stress and plastic strains
             # include "correctPlasticity.H"

            dEpsG = symm(gradDU)
                      +0.5*symm(gradDU & gradU.T())
                      +0.5*symm(gradU & gradDU.T())
                      +0.5*symm(gradDU & gradDU.T());

            dSigma = 2.0*mu*(dEpsG-dEpsP) + lambda*tr(dEpsG-dEpsP)*I;

          } while
	(
	 solverPerf.initialResidual() > convergenceTolerance 
	 && iCorr < nCorr
	 );

            U += dU; 
            gradU = fvc::grad(U);
            EpsG += dEpsG;  
            EpsP += dEpsP;
            sigma += dSigma;
            F = I + gradU + gradDU;
            J = det(F);
            sigmaCauchy = ((1.0/J) * symm(F.T() & sigma & F)); 
  
       #  include  "updateFields.H"


        Info << "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
             << "  ClockTime = " << runTime.elapsedClockTime() << " s"
             << nl << endl;
    }

    Info<< "End\n" << endl;
    return(0);
}


// ************************************************************************* //
