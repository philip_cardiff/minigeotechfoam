         
    Info<< "Reading pore pressure field p\n" << endl;
    volScalarField p
    (
        IOobject
        (
            "p",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("p", dimPressure, 0)
    );

     volVectorField pGrad
    (
        IOobject
        (
            "pGrad",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        -fvc::grad(p)
    );
      
    Info<< "creating pore pressure increment field dp\n" << endl;
    volScalarField dp
    (
        IOobject
        (
            "dp",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
       dimensionedScalar("dp", dimPressure, 0)
    );
   
    Info<< "Reading displacement increment field dU\n" << endl;
    volVectorField dU
    (
        IOobject
        (
            "dU",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    volTensorField gradDU = fvc::grad(dU);

    Info<< "Creating displacement field U\n" << endl;
    volVectorField U
    (
        IOobject
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedVector("U", dimLength, vector::zero)
    );

   Info<< "Creating strain increment field dEps\n" << endl;
   volSymmTensorField dEps
    (
        IOobject
        (
            "dEps",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
      mesh,
     dimensionedSymmTensor("dEps", dimless, symmTensor::zero)     
    );

   Info<< "Creating strain field Eps\n" << endl;
   volSymmTensorField Eps
    (
        IOobject
        (
            "Eps",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
     mesh,
     dimensionedSymmTensor("Eps", dimless, symmTensor::zero)   
    );

   Info<< "Creating stress increment field dSigma\n" << endl;
   volSymmTensorField dSigma
    (
        IOobject
        (
            "dSigma",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("dSigma", dimForce/dimArea, symmTensor::zero)
    );

   Info<< "Creating stress field sigma\n" << endl;
    volSymmTensorField sigma
    (
        IOobject
        (
            "sigma",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("sigma", dimForce/dimArea, symmTensor::zero)
    );

        volScalarField P
    (
        IOobject
        (
            "P",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
       tr(sigma)/3
    );
    Info<< "Creating plastic strain increment field dEpsP\n" << endl;
    volSymmTensorField dEpsP
    (
        IOobject
        (
            "dEpsP",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("dEpsP", dimless, symmTensor::zero)
    );

    Info<< "Creating plastic strain field EpsP\n" << endl;
    volSymmTensorField EpsP
    (
        IOobject
        (
            "EpsP",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("EpsP", dimless, symmTensor::zero)
    );

    Info<< "Creating maximum principal stress field sigma_1\n" << endl;
     volScalarField sigma_1
    (
        IOobject
        (
            "sigma_1",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
       mesh,
       dimensionedScalar("sigma_1", dimForce/dimArea, 0.0)
    );

   Info<< "Creating minimum principal stress field sigma_3\n" << endl;
     volScalarField sigma_3
    (
        IOobject
        (
            "sigma_3",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedScalar("sigma_3", dimForce/dimArea, 0.0)
    );

   Info<< "Creating explicit div dSigma field divDsigmaExp\n" << endl;
     volVectorField divDsigmaExp
    (
        IOobject
        (
            "divDsigmaExp",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedVector("divDsigmaExp", dimensionSet(1, -2, -2, 0, 0, 0, 0), vector::zero)
    );
    
     volScalarField yieldFlag
    (
        IOobject
        (
            "yieldFlag",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("yieldFlag", dimless, 0)
    );

  
    
